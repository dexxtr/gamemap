package com.example.android.gamemap;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

public class GameMapActivity extends Activity implements OnTouchListener {
    
	private GameMapView mGameMapView;
	
	private Map<Integer, Integer[]> mWays;
	private Map<Integer, Float[]> mCoords;
	
	private Integer mCurPoint = 1;
	private Integer mWidth;
	private Integer mHeight;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        initWays();
        initCoords();
        
        mGameMapView = new GameMapView(this);
        mGameMapView.setOnTouchListener(this);
        setContentView(mGameMapView);
    }
    
    @Override
	protected void onStart() {
		super.onStart();
		
		Display display = getWindowManager().getDefaultDisplay();
		
		mWidth = display.getWidth();
		mHeight = display.getHeight();
		
		mGameMapView.setDisplaySize(mWidth, mHeight);
		mGameMapView.setGameMap(this.resizedGameMap());
		
		Float[]	curCoord = mCoords.get(mCurPoint);
		mGameMapView.setCurrentCoords(curCoord[0] * mWidth, curCoord[1] * mHeight);
	}
    
    @Override
    public boolean onTouch(View v, MotionEvent event) {
    	
    	float touchX = event.getX();
		float touchY = event.getY();
		
		float touchXmin = touchX - 15;
		float touchXmax = touchX + 15;
		
		float touchYmin = touchY - 15;
		float touchYmax = touchY + 15;
		
		Float[]	curCoord = mCoords.get(mCurPoint);
		float coordX = curCoord[0] * mWidth;
		float coordY = curCoord[1] * mHeight;
		
		if ((coordX >= touchXmin) && (coordX <= touchXmax) && (coordY >= touchYmin) && (coordY <= touchYmax)) {
			showDialog(mCurPoint);
		}
		
		return true;
    	
    }
    
    protected Bitmap resizedGameMap() {
    	
    	Bitmap map = BitmapFactory.decodeResource(this.getResources(), R.drawable.map);
		int mapWidth = map.getWidth();
		int mapHeight = map.getHeight();
		
		float scaleX = (float)(mWidth)/mapWidth;
		float scaleY = (float)(mHeight)/mapHeight;
		
		Matrix matrix = new Matrix();
		matrix.postScale(scaleX, scaleY);
		
		return Bitmap.createBitmap(map, 0, 0, mapWidth, mapHeight, matrix, true);
    } 
	
    @Override
	protected Dialog onCreateDialog(int id) {
		Integer[] curWays = mWays.get(id);
		String[] curWaysStr = new String[curWays.length];
		for (int i = 0; i < curWays.length; i++) {
			curWaysStr[i] = String.valueOf(curWays[i]);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Change a point:");
		builder.setItems(curWaysStr, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mCurPoint = mWays.get(mCurPoint)[which];
				
				Float[]	curCoord = mCoords.get(mCurPoint);
				mGameMapView.setCurrentCoords(curCoord[0] * mWidth, curCoord[1] * mHeight);
				
				mGameMapView.invalidate();
			}
		});
		return builder.create();
	}

	protected void initCoords() {
		
		mCoords = new HashMap<Integer, Float[]>();
        mCoords.put(1,  new Float[] {(float) 0.77, (float) 0.77});
        mCoords.put(2,  new Float[] {(float) 0.47, (float) 0.73});
        mCoords.put(3,  new Float[] {(float) 0.52, (float) 0.61});
        mCoords.put(4,  new Float[] {(float) 0.66, (float) 0.61});
        mCoords.put(5,  new Float[] {(float) 0.81, (float) 0.60});
        mCoords.put(6,  new Float[] {(float) 0.61, (float) 0.47});
        mCoords.put(7,  new Float[] {(float) 0.35, (float) 0.64});
        mCoords.put(8,  new Float[] {(float) 0.47, (float) 0.34});
        mCoords.put(9,  new Float[] {(float) 0.73, (float) 0.45});
        mCoords.put(10, new Float[] {(float) 0.80, (float) 0.28});
        mCoords.put(11, new Float[] {(float) 0.61, (float) 0.35});
        mCoords.put(12, new Float[] {(float) 0.57, (float) 0.11});
        
	}
	
	protected void initWays() {
		
		mWays = new HashMap<Integer, Integer[]>();
        mWays.put(1,  new Integer[] {2});
        mWays.put(2,  new Integer[] {1, 3, 4});
        mWays.put(3,  new Integer[] {2});
        mWays.put(4,  new Integer[] {2, 5, 6, 9});
        mWays.put(5,  new Integer[] {4});
        mWays.put(6,  new Integer[] {4, 7, 8});
        mWays.put(7,  new Integer[] {6});
        mWays.put(8,  new Integer[] {6});
        mWays.put(9,  new Integer[] {4, 10});
        mWays.put(10, new Integer[] {9, 11, 12});
        mWays.put(11, new Integer[] {10});
        mWays.put(12, new Integer[] {10});
		
	}
}