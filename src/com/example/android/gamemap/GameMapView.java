package com.example.android.gamemap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class GameMapView extends View {

	private Integer mWidth = 0;
	private Integer mHeight = 0;
	
	private Bitmap gameMap = null;
	
	private float coordX = 0;
	private float coordY = 0;
	
	
	public GameMapView(Context context) {
		super(context);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		canvas.setViewport(mWidth, mHeight);
		canvas.drawBitmap(gameMap, 0, 0, null);
		
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		canvas.drawRect(coordX - 15, coordY - 15, coordX + 15, coordY + 15, paint);
	}

	public void setCurrentCoords(float x, float y) {
		this.coordX = x;
		this.coordY = y;
	}
	
	public void setGameMap (Bitmap gameMap) {
		this.gameMap = gameMap;
	}
	
	public void setDisplaySize(Integer width, Integer height) {
		
		this.mWidth = width;
		this.mHeight = height;
	}
}